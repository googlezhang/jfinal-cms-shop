package com.gz.controller;

import com.gz.common.AttachmentService;
import com.gz.common.Constant;
import com.gz.common.SystemService;
import com.gz.common.model.Attachment;
import com.gz.common.model.SystemParam;
import com.gz.utils.FileUtil;
import com.gz.utils.Response;
import com.jfinal.core.Controller;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by gongzhen on 2018/6/6.
 */
public class FileToolController extends Controller{
    public void upload( ) {
        UploadFile file=getFile("file", "\\temp");
        if(file==null){
            renderJson(Response.responseJson(1,"没有发现文件"));
            return;
        }
        Attachment attachment= AttachmentService.getService().getAttachmentByFile(file);
        SystemParam systemParam= SystemService.getService().getSystem("aliobs");
        SystemParam storage= SystemService.getService().getSystem("storage");
        Map<String,Object> result= new HashMap<>();
        if(storage!=null&&storage.getValue().equals("obs")){
            Map<String,String> config=new HashMap<>();
            config.put("endpoint",systemParam.getValue1());
            config.put("accessKeyId",systemParam.getValue2());
            config.put("accessKeySecret",systemParam.getValue3());
            config.put("bucketName",systemParam.getValue4());
            result= FileUtil.AliOBSUpload(config,file);
        }else{
            result= FileUtil.upload(file,Constant.FILE_UPLOAD_PATH);
        }
            if(result.get("code").equals(0)){
            attachment.setUrl(result.get("url").toString());
            attachment.save();
            result.put("attachment",attachment);
            result.put("url", Constant.FILE_PATH+attachment.getUrl());
            renderJson(result);
        }else{
            renderJson(result);
        }
    }
    public void getAttachmentList(){
        Page<Attachment> attachmentPage= AttachmentService.getService().getAttachmentPage(getParaToInt("pageNum",1),getParaToInt("pageSize",1));
        renderJson(Response.responseJson(1,"请求成功",attachmentPage));
    }
    public void delAttachment(){
        int id=getParaToInt("id",0);
        Attachment.dao.deleteById(id);
        renderJson(Response.responseJson(1,"删除成功"));
    }
}
